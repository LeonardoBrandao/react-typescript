# Basic tsx app
This is meant to be a starter project with some stuff to make bootstraping a React project with TS and Babel easier further down the line

# How to run

clone the project and change directory

`git clone https://gitlab.com/LeonardoBrandao/react-typescript.git`

`cd react-typescript`

install dependencies with `npm i`

run server with `npm run start`


open browser at [localhost:8080](http://localhost:8080)