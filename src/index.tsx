import React from 'react';
import App from './components/App';
import reducers from './reducers';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

render(

  <Provider store={createStore(reducers)}>
    <App />
  </Provider>,
  document.getElementById('app') as HTMLElement
);

