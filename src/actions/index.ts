export function fetchPokemon(pokemon: object) {
    return {
        type: 'FETCH_POKEMON',
        payload: pokemon
    }
}