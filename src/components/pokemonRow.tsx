import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

export class PokemonRow extends Component<any, any> {
    constructor(props: any) {
        super(props);
    }

    getLink(children) {
        return <NavLink to={`/${this.props.pokemon.id}`}>{children}</NavLink>
    }

    render() {
        const pokemon = {
            number: this.props.pokemon.id,
            sprite: this.props.pokemon.sprites.front_default,
            name: this.props.pokemon.name,
            type: this.props.pokemon.types[0].type.name
        }
        return <tr>
            <td>{this.getLink(pokemon.number)}</td>
            <td>
                {this.getLink(<img src={pokemon.sprite}></img>)}
            </td>
            <td>{this.getLink(pokemon.name.charAt(0).toUpperCase() + pokemon.name.slice(1))}</td>
            <td>{this.getLink(pokemon.type.charAt(0).toUpperCase() + pokemon.type.slice(1))}</td>
        </tr>

    }
}
