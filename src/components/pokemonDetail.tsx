import React, { Component } from 'react';
import { connect } from 'react-redux';

class PokemonDetail extends Component<any, any> {
    render() {
        return <h1>{this.props.pokemon.id}</h1>
    }
}

const mapStateToProps = (state, ownProps) => {
    const pokemon = state.pokemons.find(p => {
        console.log(p.id)
        console.log(ownProps.match.params.id)
        return p.id === parseInt(ownProps.match.params.id, 10);
    })
    console.log(pokemon);
    return { pokemon, ownProps };
}

export default connect(mapStateToProps)(PokemonDetail);