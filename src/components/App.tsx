import { BrowserRouter as Router, Route } from 'react-router-dom'
import Container from 'react-bootstrap/Container'
import PokemonDetail from './pokemonDetail';
import PokemonInput from './pokemonInput';
import PokemonTable from './pokemonTable';
import React from 'react';

export default class extends React.Component {
  render() {
    return <Container className='pt-5'>
      <PokemonInput />
      <Router>
        <Route path="/" exact component={PokemonTable} />
        <Route path="/:id" component={PokemonDetail} />
      </Router>
    </Container>;
  }
}