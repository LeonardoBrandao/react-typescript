import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/Table'
import { PokemonRow } from './pokemonRow';

type PokemonTableState = {
  pokemons: Array<object>,
}

class PokemonTable extends Component<PokemonTableState, any> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Sprite</th>
          <th>Name</th>
          <th>Type</th>
        </tr>
      </thead>
      <tbody>{this.props.pokemons.map((p: any) => {
        return <PokemonRow pokemon={p} key={p.number + Math.random()}></PokemonRow>
      })}</tbody>
    </Table>;
  }
}

const mapStateToProps = state => {
  return { pokemons: state.pokemons };
}

export default connect(mapStateToProps)(PokemonTable);
