import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPokemon } from '../actions';
import axios from 'axios';
import { Form, Button } from 'react-bootstrap';

class PokemonInput extends Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = { searchString: '' }
    }

    handleChange(prop: string, event: any) {
        this.setState({ [prop]: event.target.value });
    }

    handleSubmit() {
        const search = typeof this.state.searchString === 'string' ? this.state.searchString.toLowerCase() : this.state.searchString;
        axios.get(`https://pokeapi.co/api/v2/pokemon/${search}/`)
            .then(response => {
                const data = response.data;
                this.props.fetchPokemon(response.data)

            })
            .catch(err => {
                alert('Pokémon not found!');
            })
    }

    randomPokemon() {
        this.setState({ searchString: Math.floor(Math.random() * 801) + 1 }, () => this.handleSubmit());
    }


    render() {
        return <Form className='mb-3' onSubmit={(event) => {
            event.preventDefault();
            this.handleSubmit()
        }}>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Pokémon name or number</Form.Label>
                <Form.Control type="text" placeholder="Hit enter to search" value={this.state.searchString} onChange={(e) => this.handleChange('searchString', e)} />
            </Form.Group>
            <Button type='button' variant="outline-primary"
                onClick={() => this.randomPokemon()}
            >Random Pokémon!</Button>
        </Form>
    }
}
const mapStateToProps = state => {
    return { pokemon: state.pokemons };
}

export default connect(mapStateToProps, { fetchPokemon })(PokemonInput);
