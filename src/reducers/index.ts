import { combineReducers } from 'redux'

type actionType = {
    type: string,
    payload: object
}

function fetchPokemonReducer(pokemons = [], action: actionType) {
    if (action.type === 'FETCH_POKEMON') {
        return [...pokemons, action.payload];
    }
    return pokemons;
}

export default combineReducers({
    pokemons: fetchPokemonReducer,
} as any)